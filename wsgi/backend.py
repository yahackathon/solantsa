#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import pymorphy2
import os
import re
from wsgi.yadisk import YaDisk
from collections import defaultdict


APP_ROOT = os.path.dirname(os.path.abspath(__file__))


class JsonException(Exception):
    pass


class FormatParser:
    def __init__(self, encoding="utf-8"):
        self.encoding = encoding

    def parse(self, s):
        return json.loads(s)

    def serialize(self, data):
        return json.dumps(data)


class Server:
    def __init__(self):
        self.substitutor = Substitutor(APP_ROOT + "/storage.json")
        self._newcache = defaultdict(int)
        self._oldcache = defaultdict(int)
        self._count = 0
        self._yaDisk = YaDisk()

        try:
            self.format_parser = FormatParser("utf-8")
        except Exception as e:
            raise JsonException()

    def process(self, data):
        parsed_data = self.format_parser.parse(data)
        result = {}
        for id, item in parsed_data.items():
            subs = self.substitutor.process_ingredient(item)
            if subs:
                for k, v in subs.items():
                    self._newcache[v] += 1
                    self._oldcache[k] += 1

                result[id] = subs

        serialized = self.format_parser.serialize(result)

        self._count += 1
        if self._count > 10:
            self._yaDisk.save_caches(self._newcache, self._oldcache)
            self._newcache = defaultdict(int)
            self._oldcache = defaultdict(int)
            self._count = 0

        return serialized


class Substitutor:
    def __init__(self, file):
        self.matcher = Matcher(file)
        self.normalizer = pymorphy2.MorphAnalyzer()
        self.format_parser = FormatParser("utf-8")

    def normalize_word(self, s):
        parsed = self.normalizer.parse(s)
        normalized = parsed[0].normal_form
        return normalized

    def process_ingredient(self, s):
        s = s.split()
        normalized = [self.normalize_word(word) for word in s]
        i = 0
        result = {}
        while i < len(s):
            sub, order = self.matcher.get_longest_match(normalized[i:])
            if sub:
                old = " ".join(s[i:i+order])
                new = sub
                result[old] = new
                i += order
            else:
                i += 1
        return result


class Matcher:
    def __init__(self, file):
        self.table = json.load(open(file, encoding="utf-8"))
        self.ngram_order = 3

    def normalize_string(self, s):
        s = s.lower()
        s = re.sub('[.+"\-()]', '', s)
        return s

    def get_longest_match(self, words):
        words = [self.normalize_string(word) for word in words]
        best_match, best_match_order = None, 0
        for i in range(1, self.ngram_order + 1):
            ngram = " ".join(words[:i])
            if ngram in self.table:
                best_match, best_match_order = self.table[ngram], i
        return best_match, best_match_order

if __name__ == "__main__":
    server = Server()
    for i in range(35):
        res = server.process('{ "1": "килограмм сыра пармезан и картошки", "2": "гады морские и устрицы" }')
        print(res)