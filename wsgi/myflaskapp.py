from flask import Flask
from flask import request
from flask import make_response
from wsgi.backend import Server
from wsgi.backend import JsonException
from wsgi.yadisk import YaDisk


app = Flask(__name__)
server = Server()
yaDisk = YaDisk()
BASE_YANDEX_URL = "https://money.yandex.ru/quickpay/confirm.xml?quickpay-form=small&targets=%D0%97%D0%B0%D1%8F%D0%B2%D0%BA%D0%B0+%D0%BD%D0%B0+%D0%BF%D0%BE%D0%B8%D1%81%D0%BA+%D0%B8%D0%BD%D0%B3%D1%80%D0%B5%D0%B4%D0%B8%D0%B5%D0%BD%D1%82%D0%BE%D0%B2&need-phone=true&receiver=41001434795730&sum=0.1&label="


def make_flask_response(result):
    response = make_response(result)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def run_query(func, data=None, additional_data=None):
    try:
        if data is None:
            data = request.form['data']

        if data is None:
            return make_flask_response("Provide some data for parse")
        else:
            if additional_data is None:
                return make_flask_response(func(data))
            else:
                return make_flask_response(func(data, additional_data))

    except JsonException as e:
        return make_flask_response("Json parse failed <br/>" + str(e) + "<br/>" + request.form['data'])
    except Exception as e:
        return make_flask_response("Unexpected exception <br/>" + str(e) + "<br/>" + request.form['data'])
    except object as e:
        return make_flask_response("Unexpected exception <br/>" + str(e) + "<br/>" + request.form['data'])


@app.route("/", methods=['POST'])
def run_the_morpher():
    return run_query(server.process)

@app.route("/request", methods=["POST"])
def make_request():
    return run_query(yaDisk.save_request)

@app.route("/request_redirect", methods=["POST"])
def take_money_from_user():
    try:
        data = request.form['data']

        if data is None:
            return make_flask_response("Provide some data for parse")

        request_id = yaDisk.save_request(data)
        response = make_response('', 303)
        response.headers.add("Location", BASE_YANDEX_URL + request_id)
        return response

    except JsonException as e:
        return make_flask_response("Json parse failed <br/>" + str(e) + "<br/>" + request.form['data'])
    except Exception as e:
        return make_flask_response("Unexpected exception <br/>" + str(e) + "<br/>" + request.form['data'])
    except object as e:
        return make_flask_response("Unexpected exception <br/>" + str(e) + "<br/>" + request.form['data'])

@app.route("/paymentReceived", methods=["POST"])
def payment_received():
    operation_id = request.form['operation_id']
    if operation_id is None:
        operation_id = "Null"

    data = 'OperationId: ' + operation_id + '\n'
    order_id = request.form['label']
    if order_id is None:
        order_id = "Null"

    data += 'Label: ' + order_id + '\n'

    phone = request.form["phone"]
    if phone is None:
        phone = "Null"

    data += 'Phone: ' + phone + '\n'

    return run_query(yaDisk.save_payment, data, order_id)

if __name__ == "__main__":
    app.run(debug=True)