# -*- coding: utf-8 -*-

import hashlib
import base64
from datetime import datetime
from os import urandom
from tempfile import NamedTemporaryFile
from yandexwebdav import Config, remote, _encode_utf8

__author__ = 'aensidhe'


class YaDisk:
    LOGIN = "Solantsa"
    PASS = "SolantsaSolantsa"
    URL = "https://webdav.yandex.ru"
    FOLDERS = ["requests", "payments_history", "paid_requests", "cache"]

    def __init__(self, login=None, password=None):
        self._login = login
        self._password = password
        if self._login is None:
            self._login = self.LOGIN
        if self._password is None:
            self._password = self.PASS
        self._client = Config({"user": self._login, "password": self._password})
        self.check_folders()

    def check_folders(self):
        folders_on_drive = self._client.list("/")[0]
        if folders_on_drive is None:
            folders_on_drive = {}

        for folder in self.FOLDERS:
            name = "/" + folder + "/"
            if name in folders_on_drive:
                continue
            self._client.mkdir(name)

    def save_file(self, name: str, content):
        with NamedTemporaryFile() as tmp:
            tmp.write(content)
            tmp.seek(0)
            self._client.upload(tmp.name, name)

    def save_request(self, content: str):
        content = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f") + "\nRandom sequence: " + str(base64.b64encode(urandom(16))) + "\n" + content
        b = content.encode("utf-8")
        filename = hashlib.md5(b).hexdigest()
        self.save_file("requests/" + filename + ".txt", b)
        return filename

    def move_file(self, oldname, newname):
        oldname = remote(oldname)
        newname = remote(newname)
        conn = self._client.getConnection()
        headers = self._client.getHeaders()
        headers["Destination"] = _encode_utf8(newname)
        headers["Overwrite"] = "T"
        conn.request("MOVE", _encode_utf8(oldname), "", headers)
        conn.getresponse()

    def save_payment(self, content: str, request_label: str):
        b = content.encode("utf-8")
        filename = hashlib.md5(b).hexdigest()
        self.save_file("payments_history/" + filename + ".txt", b)

        try:
            self.move_file("requests/" + request_label + ".txt", "paid_requests/" + request_label + ".txt")
        except:
            pass

        return filename

    def _save_cache(self, cache, filename):
        content = ""
        for k, v in cache.items():
            content += str(k) + ": " + str(v) + "\n"
        self.save_file("/cache/" + filename, content.encode())

    def save_caches(self, new_cache, old_cache):
        self._save_cache(new_cache, datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f") + ".new.cache.txt")
        self._save_cache(old_cache, datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f") + ".old.cache.txt")

    pass


if __name__ == "__main__":
    disk = YaDisk()
    disk.move_file("/A/package.json", "/B/package.json")
