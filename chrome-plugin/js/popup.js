function renderDataLocal(listData) {
    if (typeof listData != 'object') {
        window.close();
        return false;
    }

    var list = [];
    var _list = {};
    $.each(listData, function(i, item) {
        $.each(item, function(key, val) {
            _list[val] = 1;
        });
    });

    $.each(_list, function(key) {
        list.push(key);
    });

    if (list.length == 0) {
        $('#block_fail').show(0);
        window.setTimeout(function() {
            window.close();
        }, 4000);
    } else {
        $.each(list, function(i, val) {
            $('#listProduct').append('<div class="product_item"><label><input name="product[]" type="checkbox" checked="checked" value="'+ htmlspecialchars(val) +'">'+ htmlspecialchars(val) +'</label></div>');
        });
        $('#block_success').show(0);
    }
    $('#block_result').show(0);

    $('#loading').hide(0, function() {
        $('#block_result').show();
    });
}

$(document).ready(function() {
    $('#block_success .b_order').bind('click', function() {
        order();
    });
});

function order() {
    $('.msg').hide(0);
    $('#msgError').html('');
    $('#msgSuccess').html('');

    if ($('#listProduct input[name^=product]:checked').size() == 0) {
        $('#msgError').html('Выберите хотя бы один ингредиент.');
        $('.msg').show(0);
        return false;
    }

    if (!$('#confirmOrder').is(':checked')) {
        $('#msgError').html('Примите условия сервиса.');
        $('.msg').show(0);
        f.focus();
        return false;
    }

    var listProduct = [];
    $('#listProduct input[name^=product]:checked').each(function(i, val) {
        listProduct.push($(this).val());
    });

    $('#formPaid input[name=data]').val(JSON.stringify(listProduct));
    $('#formPaid').submit();
    window.close();

    return true;
}

window.addEventListener('DOMContentLoaded', function() {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {
        chrome.tabs.sendMessage(
            tabs[0].id,
            {from: 'popup', subject: 'getListData'}
        );
    });
});

chrome.runtime.onMessage.addListener(function (msg, sender, response) {
    if (msg.from === 'content' && msg.subject === 'sendListData') {
        renderDataLocal(msg.data);
    }
});