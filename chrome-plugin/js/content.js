var solantsa = {
	cacheData: null,
	cacheAjaxData: null,
	isRunRender: false,

	listType: {
		selectors: ['[itemprop=ingredients] .ingredient-name', '[itemprop=ingredients]', '.ingredient'],

		default: {
			getData: function () {
				return {};
			}
		}
	},

	getType: function () {
		var type = this.listType.default;

		this.listType.selectors.forEach(function (el) {
			if ($(el).size() > 0) {
				type = {
					getData: function () {
						var list = {};
						$(el).each(function (i, item) {
							list[i] = $(item).html();
							$(item).attr('data-solantsa-key', i);

						});
						return list;
					}
				}
			}
		});

		return type;
	},

	getListData: function () {
		if (this.cacheData) {
			return this.cacheData;
		}
		this.cacheData = this.getType().getData();

		return this.cacheData;
	},

	loadAjaxData: function (callback) {
		var $this = this;

        if ($this.cacheAjaxData) {
            callback($this);
            return true;
        }
		this.getListData();

		$.ajax({
			url: "https://morph-solantsa.rhcloud.com",
			data: {
				data: JSON.stringify(this.cacheData)
			},
			type: 'post',
			dataType: 'json',
			success: function (data) {
				$this.cacheAjaxData = data;
				callback($this);
			},
			error: function (i, textStatus) {
				console.log([i, textStatus]);
				callback($this);
			}
		});

		return true;
	},
	renderData: function (t) {
        var $this = (typeof t == 'undefined')? this: t;

		if (!$this.cacheAjaxData) {
			return false;
		}

		if ($this.isRunRender) {
			return true;
		}
        $this.isRunRender = true;

		$.each($this.cacheAjaxData, function (i, item) {
			var t = $('[data-solantsa-key=' + i + ']');
			var html = t.html();
			$.each(item, function (key, val) {
				html = html.replace(key, '<s style="background-color: rgb(248, 209, 209);">' + key + '</s> <span style="background-color:  rgb(207, 207, 255);;">' + val + '</span> ');
			});
			t.html(html);
		});

		return true;
	},
	preLoad: function () {
        console.log('preLoad');
        this.loadAjaxData(this.renderData);
	}
};

chrome.runtime.onMessage.addListener(function (msg, sender, response) {
    if (msg.from === 'popup' && msg.subject === 'getListData') {
        //console.log(solantsa.cacheAjaxData);
        (function () {
            solantsa.loadAjaxData(function () {
                solantsa.renderData();
                chrome.runtime.sendMessage({
                    from: 'content',
                    subject: 'sendListData',
                    data: solantsa.cacheAjaxData
                });
            });
        })();
    }
});

$(document).ready(function () {
    chrome.storage.sync.get({
        process_every_page_auto: false
    }, function(item) {
        if (item.process_every_page_auto) {
            solantsa.preLoad();
        }
    });
});