// Saves options to chrome.storage
function save_options() {
    var process_every_page_auto = $('#process_every_page_auto').is(':checked');

    chrome.storage.sync.set({
        process_every_page_auto: process_every_page_auto
    }, function() {
        // Update status to let user know options were saved.
        var status = document.getElementById('status');
        status.textContent = 'Изменения сохранены.';
        setTimeout(function() {
            status.textContent = '';
        }, 2000);
    });
}

function restore_options() {
    chrome.storage.sync.get({
        process_every_page_auto: false
    }, function(items) {
        $('#process_every_page_auto').prop('checked', items.process_every_page_auto);
    });
}

document.addEventListener('DOMContentLoaded', function() {
    restore_options();
    document.getElementById('save').addEventListener('click', save_options);
});