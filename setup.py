from setuptools import setup

setup(name='Solantsa',
      version='1.0',
      description='OpenShift App',
      author='Solantsa team',
      author_email='example@example.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask>=0.7.2', 'MarkupSafe', 'pymorphy2', "yandexwebdav"])

